@extends('posts.master')
@section('blog_content')
  @include('posts.layouts.blog_post')
  <hr>
  <section class="comments">
    <ul class="list-group">
      @foreach ($post->comments as $coment)
        <strong>{{comment->created_at->diffForHumans()}}</strong>
        <li class="list-group">
          {{$comment->body}}
        </li class="list-group-item">
      @endforeach
    </ul>
  </section>
@endsection
