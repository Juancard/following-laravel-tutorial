<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>
          @yield('head_title')
        </title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <!-- Bootstrap core CSS -->
        <!-- Not needed anymore-->
        <!--link href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet"-->
        <link href="/css/blog.css" rel="stylesheet">
        <link href="/css/app.css" rel="stylesheet">
        @yield('styles')
    </head>
    <body>
      @include('layouts.nav')
      @yield('content')
      @include('layouts.footer')
      <!-- Placed at the end of the document so the pages load faster -->
      @include('layouts.common_scripts')
      @yield('scripts_at_end')
    </body>
</html>
