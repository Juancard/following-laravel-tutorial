@extends('posts.master')
@section('blog_content')
  @foreach ($posts as $post)
    @include('posts.layouts.blog_post')
  @endforeach
@endsection
