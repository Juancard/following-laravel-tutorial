@extends('layouts.master')

@section('head_title')
  Blog
@endsection

@section('styles')
@endsection

@section('content')
  @include('posts.layouts.blog_header')
  <div class="container">
    <div class="row">
      <div class="col-sm-8 blog-main">
        @yield('blog_content')
      </div>
      <div class="col-sm-4 offset-sm-1 blog-sidebar">
        @include('posts.layouts.blog_sidebar')
      </div><!-- /.blog-sidebar -->
    </div>
  </div>
@endsection
